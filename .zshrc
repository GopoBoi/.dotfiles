zstyle ':completion:*' completer _expand _complete _ignored _approximate
zstyle :compinstall filename '/home/gopoboi/.zshrc'

autoload -Uz compinit 
if [[ -n ${ZDOTDIR}/.zcompdump(#qN.mh+24) ]]; then
	compinit;
else
	compinit -C;
fi;
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt autocd
unsetopt beep
bindkey -v

path+='/home/gopoboi/.cargo/bin'
path+='/home/gopoboi/.rustup/toolchains/nightly-x86_64-unknown-linux-gnu/bin'
path+='/home/gopoboi/dotfiles/scripts'
export PATH


alias vi="nvim"
alias zshrc="nvim ~/.zshrc"
alias ll='ls -l --color=auto'

eval "$(starship init zsh)"
