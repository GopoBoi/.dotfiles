#!/bin/bash
# baraction.sh script for spectrwm status bar

SLEEP_SEC=5  # set bar_delay = 5 in /etc/spectrwm.conf
PREFIX='Playing: ' # what should go before the now playing string
SUFFIX='' # what should go after the now playing string
# save the now playing string from the mpd client of choice (it and 
# format can be of course changed according to one's tastes
#loops forever outputting a line every SLEEP_SEC secs
while :; do
    NP=$(ncmpcpp --current-song "%f")
    BAT=$(cat /sys/class/power_supply/BAT1/capacity)

    if [ -z $NP ]; then
        echo "Nothing is currently playing"
    fi

    STATUS="${BAT} ${PREFIX}${NP}${SUFFIX}"
    echo $STATUS
    ttytter -rc=-donearm -script -status="$STATUS"
    sleep $SLEEP_SEC
done
