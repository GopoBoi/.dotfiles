vim.g.mapleader = " "

vim.opt.nu = true
vim.opt.relativenumber = true
vim.opt.laststatus = 2

vim.opt.errorbells = false

vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true

vim.opt.smartindent = true

vim.opt.wrap = false

vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.undofile = true

vim.opt.hlsearch = false
vim.opt.incsearch = true

vim.opt.termguicolors = true

vim.opt.scrolloff = 10
vim.opt.signcolumn = "yes"
vim.opt.isfname:append("@-@")

vim.opt.cmdheight = 0

vim.opt.updatetime = 50

-- Enable transparent mode
-- vim.g.gruvbox_baby_background_color = "dark"
-- vim.g.gruvbox_baby_transparent_mode = 1
-- vim.cmd.colorscheme "gruvbox-baby"

-- vim.cmd.colorscheme "abscs"

-- vim.g.oxocarbon_lua_alternative_telescope = true
-- vim.g.oxocarbon_lua_transparent = true
-- vim.cmd.colorscheme "oxocarbon-lua"

-- vim.cmd.colorscheme "kanagawa"

-- vim.g.catppuccin_flavour = "mocha"
-- vim.cmd.colorscheme "catppuccinet

-- vim.cmd.colorscheme "tokyodark"

-- vim.cmd("hi! Normal ctermbg=NONE guibg=NONE")
-- vim.cmd("hi! NonText ctermbg=NONE guibg=NONE")
