return require("packer").startup(function(use)
  -- Packer can manage itself
    use("wbthomason/packer.nvim")

    -- some libs
    use("nvim-lua/plenary.nvim")
    use("nvim-lua/popup.nvim")

    -- file stuffs
    use("nvim-telescope/telescope.nvim")
    use("kyazdani42/nvim-tree.lua")

    -- language support
    use("nvim-treesitter/nvim-treesitter", {run = ":TSUpdate"})
    use("nvim-treesitter/nvim-treesitter-textobjects")
    use("nvim-treesitter/nvim-treesitter-context")
    use("neovim/nvim-lspconfig")
    use("L3MON4D3/LuaSnip")
    use("onsails/lspkind.nvim")
    use("hrsh7th/nvim-cmp")
    use("hrsh7th/cmp-nvim-lsp")
    use("hrsh7th/cmp-buffer")
    use("hrsh7th/cmp-path")
    use("saadparwaiz1/cmp_luasnip")
    use("williamboman/mason.nvim")
    use("williamboman/mason-lspconfig.nvim")
    use("mfussenegger/nvim-dap")
    use("simrat39/rust-tools.nvim")
    use {
    "danymat/neogen",
    config = function()
        require('neogen').setup {}
    end,
    requires = "nvim-treesitter/nvim-treesitter",}

    use("numToStr/Comment.nvim")

    -- colorschemes
    use("B4mbus/oxocarbon-lua.nvim")
    use("Abstract-IDE/Abstract-cs")
    use("luisiacc/gruvbox-baby")
    use("rose-pine/neovim")

    -- visual fluff
    use("kyazdani42/nvim-web-devicons")
    use("nvim-lualine/lualine.nvim")
    use("stevearc/dressing.nvim")
    use("NvChad/nvim-colorizer.lua")
    use("p00f/nvim-ts-rainbow")
end)
