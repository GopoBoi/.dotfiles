if status is-interactive
    # Commands to run in interactive sessions can go here
end

fish_add_path -a ~/.cargo/bin/
fish_add_path -a ~/.rustup/toolchains/nightly-x86_64-unknown-linux-gnu/bin
fish_add_path -a ~/dotfiles/scripts/

function vim
    nvim
end
function 1mon
    mons -o
end
function 2mon 
    mons -e right
end
function cc99
    gcc -Wall -Wextra -pedantic -Wconversion -std=c99 $argv
end

# Start X at login
if status is-login
    if test -z "$DISPLAY" -a "$XDG_VTNR" = 1
        exec startx -- -keeptty
    end
end
